{
    "header": {
        "pipelineVersion": "2.2",
        "releaseVersion": "2021.1.0",
        "fileVersion": "1.1",
        "nodesVersions": {
            "CameraInit": "4.0",
            "MeshFiltering": "3.0",
            "FeatureMatching": "2.0",
            "Meshing": "7.0",
            "ImageMatching": "2.0",
            "DepthMapFilter": "3.0",
            "Texturing": "5.0",
            "DepthMap": "2.0",
            "PrepareDenseScene": "3.0",
            "FeatureExtraction": "1.1",
            "StructureFromMotion": "2.0"
        }
    },
    "graph": {
        "CameraInit_1": {
            "nodeType": "CameraInit",
            "position": [
                0,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 26,
                "split": 1
            },
            "uids": {
                "0": "a850879bfee3226dcd3243d7a041bf2bb632cd38"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "viewpoints": [
                    {
                        "viewId": 53331003,
                        "poseId": 53331003,
                        "path": "D:/Projects/rustam2/IMG20221221184300.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:00\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:00\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:00\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"241\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"178000\", \"Exif:SubsecTimeDigitized\": \"178000\", \"Exif:SubsecTimeOriginal\": \"178000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 138511863,
                        "poseId": 138511863,
                        "path": "D:/Projects/rustam2/IMG20221221184329.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:29\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:29\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:29\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"206\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"236000\", \"Exif:SubsecTimeDigitized\": \"236000\", \"Exif:SubsecTimeOriginal\": \"236000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 252026981,
                        "poseId": 252026981,
                        "path": "D:/Projects/rustam2/IMG20221221184358.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:58\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:58\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:58\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"225\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"91000\", \"Exif:SubsecTimeDigitized\": \"91000\", \"Exif:SubsecTimeOriginal\": \"91000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 272170678,
                        "poseId": 272170678,
                        "path": "D:/Projects/rustam2/IMG20221221184339.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:39\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:39\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:39\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"300\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"985000\", \"Exif:SubsecTimeDigitized\": \"985000\", \"Exif:SubsecTimeOriginal\": \"985000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 287492699,
                        "poseId": 287492699,
                        "path": "D:/Projects/rustam2/IMG20221221184323.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:23\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:23\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:23\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"213\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"375000\", \"Exif:SubsecTimeDigitized\": \"375000\", \"Exif:SubsecTimeOriginal\": \"375000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 447871668,
                        "poseId": 447871668,
                        "path": "D:/Projects/rustam2/IMG20221221184304.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:04\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:04\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:04\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"241\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"919000\", \"Exif:SubsecTimeDigitized\": \"919000\", \"Exif:SubsecTimeOriginal\": \"919000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 543715402,
                        "poseId": 543715402,
                        "path": "D:/Projects/rustam2/IMG20221221184302.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:02\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:02\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:02\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"250\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"534000\", \"Exif:SubsecTimeDigitized\": \"534000\", \"Exif:SubsecTimeOriginal\": \"534000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 669165519,
                        "poseId": 669165519,
                        "path": "D:/Projects/rustam2/IMG20221221184315.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:15\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:15\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:15\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"391\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"260000\", \"Exif:SubsecTimeDigitized\": \"260000\", \"Exif:SubsecTimeOriginal\": \"260000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 724023031,
                        "poseId": 724023031,
                        "path": "D:/Projects/rustam2/IMG20221221184321.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:21\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:21\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:21\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"233\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"71000\", \"Exif:SubsecTimeDigitized\": \"71000\", \"Exif:SubsecTimeOriginal\": \"71000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 743250649,
                        "poseId": 743250649,
                        "path": "D:/Projects/rustam2/IMG20221221184355.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:55\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:55\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:55\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"244\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"880000\", \"Exif:SubsecTimeDigitized\": \"880000\", \"Exif:SubsecTimeOriginal\": \"880000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 786049479,
                        "poseId": 786049479,
                        "path": "D:/Projects/rustam2/IMG20221221184349.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:49\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:49\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:49\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"328\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"10000\", \"Exif:SubsecTimeDigitized\": \"10000\", \"Exif:SubsecTimeOriginal\": \"10000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 897431487,
                        "poseId": 897431487,
                        "path": "D:/Projects/rustam2/IMG20221221184257.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:42:57\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:42:57\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:42:57\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"233\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"354000\", \"Exif:SubsecTimeDigitized\": \"354000\", \"Exif:SubsecTimeOriginal\": \"354000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 941253820,
                        "poseId": 941253820,
                        "path": "D:/Projects/rustam2/IMG20221221184346.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:46\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:46\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:46\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"338\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"20000\", \"Exif:SubsecTimeDigitized\": \"20000\", \"Exif:SubsecTimeOriginal\": \"20000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1095220688,
                        "poseId": 1095220688,
                        "path": "D:/Projects/rustam2/IMG20221221184352.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:52\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:52\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:52\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"328\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"177000\", \"Exif:SubsecTimeDigitized\": \"177000\", \"Exif:SubsecTimeOriginal\": \"177000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1209058063,
                        "poseId": 1209058063,
                        "path": "D:/Projects/rustam2/IMG20221221184343.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:43\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:43\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:43\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"309\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"222000\", \"Exif:SubsecTimeDigitized\": \"222000\", \"Exif:SubsecTimeOriginal\": \"222000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1261672870,
                        "poseId": 1261672870,
                        "path": "D:/Projects/rustam2/IMG20221221184309.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:09\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:09\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:09\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"291\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"251000\", \"Exif:SubsecTimeDigitized\": \"251000\", \"Exif:SubsecTimeOriginal\": \"251000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1276062222,
                        "poseId": 1276062222,
                        "path": "D:/Projects/rustam2/IMG20221221184353.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:53\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:53\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:53\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"319\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"191000\", \"Exif:SubsecTimeDigitized\": \"191000\", \"Exif:SubsecTimeOriginal\": \"191000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1300359665,
                        "poseId": 1300359665,
                        "path": "D:/Projects/rustam2/IMG20221221184332.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:32\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:32\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:32\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"250\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"711000\", \"Exif:SubsecTimeDigitized\": \"711000\", \"Exif:SubsecTimeOriginal\": \"711000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1394737391,
                        "poseId": 1394737391,
                        "path": "D:/Projects/rustam2/IMG20221221184337.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:37\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:37\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:37\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"328\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"488000\", \"Exif:SubsecTimeDigitized\": \"488000\", \"Exif:SubsecTimeOriginal\": \"488000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1433896418,
                        "poseId": 1433896418,
                        "path": "D:/Projects/rustam2/IMG20221221184335.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:35\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:35\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:35\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"286\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"204000\", \"Exif:SubsecTimeDigitized\": \"204000\", \"Exif:SubsecTimeOriginal\": \"204000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1471417863,
                        "poseId": 1471417863,
                        "path": "D:/Projects/rustam2/IMG20221221184325.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:25\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:25\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:25\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"200\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"639000\", \"Exif:SubsecTimeDigitized\": \"639000\", \"Exif:SubsecTimeOriginal\": \"639000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1510431903,
                        "poseId": 1510431903,
                        "path": "D:/Projects/rustam2/IMG20221221184307.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:07\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:07\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:07\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"278\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"185000\", \"Exif:SubsecTimeDigitized\": \"185000\", \"Exif:SubsecTimeOriginal\": \"185000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1622416689,
                        "poseId": 1622416689,
                        "path": "D:/Projects/rustam2/IMG20221221184311.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:11\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:11\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:11\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"300\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"350000\", \"Exif:SubsecTimeDigitized\": \"350000\", \"Exif:SubsecTimeOriginal\": \"350000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1672790601,
                        "poseId": 1672790601,
                        "path": "D:/Projects/rustam2/IMG20221221184400.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:44:00\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:44:00\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:44:00\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"122\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"350000\", \"Exif:SubsecTimeDigitized\": \"350000\", \"Exif:SubsecTimeOriginal\": \"350000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1931626043,
                        "poseId": 1931626043,
                        "path": "D:/Projects/rustam2/IMG20221221184254.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:42:54\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:42:54\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:42:54\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"125\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"179000\", \"Exif:SubsecTimeDigitized\": \"179000\", \"Exif:SubsecTimeOriginal\": \"179000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 2106307049,
                        "poseId": 2106307049,
                        "path": "D:/Projects/rustam2/IMG20221221184318.jpg",
                        "intrinsicId": 3545769884,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2022:12:21 18:43:18\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2022:12:21 18:43:18\", \"Exif:DateTimeOriginal\": \"2022:12:21 18:43:18\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"291\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"423000\", \"Exif:SubsecTimeDigitized\": \"423000\", \"Exif:SubsecTimeOriginal\": \"423000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.599\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2022:12:21\", \"GPS:Latitude\": \"53, 38, 14.37\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 47.29\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"13, 40, 53\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    }
                ],
                "intrinsics": [
                    {
                        "intrinsicId": 3545769884,
                        "pxInitialFocalLength": -1.0,
                        "pxFocalLength": 5562.348047707611,
                        "type": "fisheye4",
                        "width": 3456,
                        "height": 4608,
                        "sensorWidth": -1.0,
                        "sensorHeight": -0.75,
                        "serialNumber": "D:/Projects/rustam2_realme_realme 6 Pro",
                        "principalPoint": {
                            "x": 1728.0,
                            "y": 2304.0
                        },
                        "initializationMode": "unknown",
                        "distortionParams": [
                            0.0,
                            0.0,
                            0.0,
                            0.0
                        ],
                        "locked": false
                    }
                ],
                "sensorDatabase": "D:\\Programms\\New Programms\\Meshroom\\Meshroom-2021.1.0-win64\\Meshroom-2021.1.0\\aliceVision\\share\\aliceVision\\cameraSensors.db",
                "defaultFieldOfView": 45.0,
                "groupCameraFallback": "folder",
                "allowedCameraModels": [
                    "pinhole",
                    "radial1",
                    "radial3",
                    "brown",
                    "fisheye4",
                    "fisheye1"
                ],
                "useInternalWhiteBalance": true,
                "viewIdMethod": "metadata",
                "viewIdRegex": ".*?(\\d+)",
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/cameraInit.sfm"
            }
        },
        "FeatureExtraction_1": {
            "nodeType": "FeatureExtraction",
            "position": [
                200,
                0
            ],
            "parallelization": {
                "blockSize": 40,
                "size": 26,
                "split": 1
            },
            "uids": {
                "0": "19cd2a092cfeb0a752f8c84e7d8f1524296200fc"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{CameraInit_1.output}",
                "describerTypes": [
                    "sift",
                    "akaze"
                ],
                "describerPreset": "normal",
                "maxNbFeatures": 0,
                "describerQuality": "normal",
                "contrastFiltering": "GridSort",
                "relativePeakThreshold": 0.01,
                "gridFiltering": true,
                "forceCpuExtraction": true,
                "maxThreads": 0,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/"
            }
        },
        "ImageMatching_1": {
            "nodeType": "ImageMatching",
            "position": [
                400,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 26,
                "split": 1
            },
            "uids": {
                "0": "8c9a5559b50cb02ccbe2c58c69937d35d3cf0fa6"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{FeatureExtraction_1.input}",
                "featuresFolders": [
                    "{FeatureExtraction_1.output}"
                ],
                "method": "VocabularyTree",
                "tree": "D:\\Programms\\New Programms\\Meshroom\\Meshroom-2021.1.0-win64\\Meshroom-2021.1.0\\aliceVision\\share\\aliceVision\\vlfeat_K80L3.SIFT.tree",
                "weights": "",
                "minNbImages": 200,
                "maxDescriptors": 500,
                "nbMatches": 50,
                "nbNeighbors": 50,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/imageMatches.txt"
            }
        },
        "FeatureMatching_1": {
            "nodeType": "FeatureMatching",
            "position": [
                600,
                0
            ],
            "parallelization": {
                "blockSize": 20,
                "size": 26,
                "split": 2
            },
            "uids": {
                "0": "63cabb30cb862d9669e21f0bafa87050b2bbf3d2"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{ImageMatching_1.input}",
                "featuresFolders": "{ImageMatching_1.featuresFolders}",
                "imagePairsList": "{ImageMatching_1.output}",
                "describerTypes": "{FeatureExtraction_1.describerTypes}",
                "photometricMatchingMethod": "ANN_L2",
                "geometricEstimator": "acransac",
                "geometricFilterType": "fundamental_matrix",
                "distanceRatio": 0.8,
                "maxIteration": 2048,
                "geometricError": 0.0,
                "knownPosesGeometricErrorMax": 5.0,
                "maxMatches": 0,
                "savePutativeMatches": false,
                "crossMatching": false,
                "guidedMatching": true,
                "matchFromKnownCameraPoses": false,
                "exportDebugFiles": false,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/"
            }
        },
        "StructureFromMotion_1": {
            "nodeType": "StructureFromMotion",
            "position": [
                800,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 26,
                "split": 1
            },
            "uids": {
                "0": "a5dc32623c7642d4105976bee43603a2ba797570"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{FeatureMatching_1.input}",
                "featuresFolders": "{FeatureMatching_1.featuresFolders}",
                "matchesFolders": [
                    "{FeatureMatching_1.output}"
                ],
                "describerTypes": "{FeatureMatching_1.describerTypes}",
                "localizerEstimator": "acransac",
                "observationConstraint": "Basic",
                "localizerEstimatorMaxIterations": 4096,
                "localizerEstimatorError": 0.0,
                "lockScenePreviouslyReconstructed": false,
                "useLocalBA": true,
                "localBAGraphDistance": 1,
                "maxNumberOfMatches": 0,
                "minNumberOfMatches": 0,
                "minInputTrackLength": 2,
                "minNumberOfObservationsForTriangulation": 2,
                "minAngleForTriangulation": 3.0,
                "minAngleForLandmark": 2.0,
                "maxReprojectionError": 4.0,
                "minAngleInitialPair": 5.0,
                "maxAngleInitialPair": 40.0,
                "useOnlyMatchesFromInputFolder": false,
                "useRigConstraint": true,
                "lockAllIntrinsics": false,
                "filterTrackForks": false,
                "initialPairA": "",
                "initialPairB": "",
                "interFileExtension": ".abc",
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/sfm.abc",
                "outputViewsAndPoses": "{cache}/{nodeType}/{uid0}/cameras.sfm",
                "extraInfoFolder": "{cache}/{nodeType}/{uid0}/"
            }
        },
        "PrepareDenseScene_1": {
            "nodeType": "PrepareDenseScene",
            "position": [
                1000,
                0
            ],
            "parallelization": {
                "blockSize": 40,
                "size": 26,
                "split": 1
            },
            "uids": {
                "0": "d660208a85d4401b76058c104aa9b67540bd56ea"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{StructureFromMotion_1.output}",
                "imagesFolders": [],
                "outputFileType": "exr",
                "saveMetadata": true,
                "saveMatricesTxtFiles": false,
                "evCorrection": false,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/",
                "outputUndistorted": "{cache}/{nodeType}/{uid0}/*.{outputFileTypeValue}"
            }
        },
        "DepthMap_1": {
            "nodeType": "DepthMap",
            "position": [
                1200,
                0
            ],
            "parallelization": {
                "blockSize": 3,
                "size": 26,
                "split": 9
            },
            "uids": {
                "0": "513200db7f99973b2dfd7b6a6120ce4c6a226632"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{PrepareDenseScene_1.input}",
                "imagesFolder": "{PrepareDenseScene_1.output}",
                "downscale": 2,
                "minViewAngle": 2.0,
                "maxViewAngle": 70.0,
                "sgmMaxTCams": 10,
                "sgmWSH": 4,
                "sgmGammaC": 5.5,
                "sgmGammaP": 8.0,
                "refineMaxTCams": 6,
                "refineNSamplesHalf": 150,
                "refineNDepthsToRefine": 31,
                "refineNiters": 100,
                "refineWSH": 3,
                "refineSigma": 15,
                "refineGammaC": 15.5,
                "refineGammaP": 8.0,
                "refineUseTcOrRcPixSize": false,
                "exportIntermediateResults": false,
                "nbGPUs": 0,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/"
            }
        },
        "DepthMapFilter_1": {
            "nodeType": "DepthMapFilter",
            "position": [
                1400,
                0
            ],
            "parallelization": {
                "blockSize": 10,
                "size": 26,
                "split": 3
            },
            "uids": {
                "0": "b20e157d0c94af188baa96ad00ac98eaa5223648"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{DepthMap_1.input}",
                "depthMapsFolder": "{DepthMap_1.output}",
                "minViewAngle": 2.0,
                "maxViewAngle": 70.0,
                "nNearestCams": 10,
                "minNumOfConsistentCams": 2,
                "minNumOfConsistentCamsWithLowSimilarity": 3,
                "pixSizeBall": 0,
                "pixSizeBallWithLowSimilarity": 0,
                "computeNormalMaps": false,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/"
            }
        },
        "Meshing_1": {
            "nodeType": "Meshing",
            "position": [
                1600,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 1,
                "split": 1
            },
            "uids": {
                "0": "9522dfc3899a76f81984ae966c3e888948bcb317"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{DepthMapFilter_1.input}",
                "depthMapsFolder": "{DepthMapFilter_1.output}",
                "useBoundingBox": false,
                "boundingBox": {
                    "bboxTranslation": {
                        "x": 0.0,
                        "y": 0.0,
                        "z": 0.0
                    },
                    "bboxRotation": {
                        "x": 0.0,
                        "y": 0.0,
                        "z": 0.0
                    },
                    "bboxScale": {
                        "x": 1.0,
                        "y": 1.0,
                        "z": 1.0
                    }
                },
                "estimateSpaceFromSfM": true,
                "estimateSpaceMinObservations": 3,
                "estimateSpaceMinObservationAngle": 10,
                "maxInputPoints": 50000000,
                "maxPoints": 5000000,
                "maxPointsPerVoxel": 1000000,
                "minStep": 2,
                "partitioning": "singleBlock",
                "repartition": "multiResolution",
                "angleFactor": 15.0,
                "simFactor": 15.0,
                "pixSizeMarginInitCoef": 2.0,
                "pixSizeMarginFinalCoef": 4.0,
                "voteMarginFactor": 4.0,
                "contributeMarginFactor": 2.0,
                "simGaussianSizeInit": 10.0,
                "simGaussianSize": 10.0,
                "minAngleThreshold": 1.0,
                "refineFuse": true,
                "helperPointsGridSize": 10,
                "densify": false,
                "densifyNbFront": 1,
                "densifyNbBack": 1,
                "densifyScale": 20.0,
                "nPixelSizeBehind": 4.0,
                "fullWeight": 1.0,
                "voteFilteringForWeaklySupportedSurfaces": true,
                "addLandmarksToTheDensePointCloud": false,
                "invertTetrahedronBasedOnNeighborsNbIterations": 10,
                "minSolidAngleRatio": 0.2,
                "nbSolidAngleFilteringIterations": 2,
                "colorizeOutput": false,
                "addMaskHelperPoints": false,
                "maskHelperPointsWeight": 1.0,
                "maskBorderSize": 4,
                "maxNbConnectedHelperPoints": 50,
                "saveRawDensePointCloud": false,
                "exportDebugTetrahedralization": false,
                "seed": 0,
                "verboseLevel": "info"
            },
            "outputs": {
                "outputMesh": "{cache}/{nodeType}/{uid0}/mesh.obj",
                "output": "{cache}/{nodeType}/{uid0}/densePointCloud.abc"
            }
        },
        "MeshFiltering_1": {
            "nodeType": "MeshFiltering",
            "position": [
                1800,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 1,
                "split": 1
            },
            "uids": {
                "0": "97a2735e03fbaf5df98441ee77177bc212e748c6"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "inputMesh": "{Meshing_1.outputMesh}",
                "keepLargestMeshOnly": false,
                "smoothingSubset": "all",
                "smoothingBoundariesNeighbours": 0,
                "smoothingIterations": 5,
                "smoothingLambda": 1.0,
                "filteringSubset": "all",
                "filteringIterations": 1,
                "filterLargeTrianglesFactor": 60.0,
                "filterTrianglesRatio": 0.0,
                "verboseLevel": "info"
            },
            "outputs": {
                "outputMesh": "{cache}/{nodeType}/{uid0}/mesh.obj"
            }
        },
        "Texturing_1": {
            "nodeType": "Texturing",
            "position": [
                2000,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 1,
                "split": 1
            },
            "uids": {
                "0": "97185698dcaf9903b2d8858fc284906d13584da9"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{Meshing_1.output}",
                "imagesFolder": "{DepthMap_1.imagesFolder}",
                "inputMesh": "{MeshFiltering_1.outputMesh}",
                "textureSide": 8192,
                "downscale": 2,
                "outputTextureFileType": "png",
                "unwrapMethod": "Basic",
                "useUDIM": true,
                "fillHoles": false,
                "padding": 5,
                "multiBandDownscale": 4,
                "multiBandNbContrib": {
                    "high": 1,
                    "midHigh": 5,
                    "midLow": 10,
                    "low": 0
                },
                "useScore": true,
                "bestScoreThreshold": 0.1,
                "angleHardThreshold": 90.0,
                "processColorspace": "sRGB",
                "correctEV": false,
                "forceVisibleByAllVertices": false,
                "flipNormals": false,
                "visibilityRemappingMethod": "PullPush",
                "subdivisionTargetRatio": 0.8,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/",
                "outputMesh": "{cache}/{nodeType}/{uid0}/texturedMesh.obj",
                "outputMaterial": "{cache}/{nodeType}/{uid0}/texturedMesh.mtl",
                "outputTextures": "{cache}/{nodeType}/{uid0}/texture_*.{outputTextureFileTypeValue}"
            }
        }
    }
}